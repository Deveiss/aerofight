#pragma once

#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/UI/UIElement.h>

#include "MenuBuilder.h"

#include "Client.h"
#include "Server.h"

using namespace Urho3D;

class Aerofight : public Urho3D::Application {
		URHO3D_OBJECT(Aerofight, Application);
public:
	Aerofight(Urho3D::Context* context) : Application(context) {}

	virtual void Setup();
	virtual void Start();
	virtual void Stop();

	void SetupServer();
	void DestroyServer();

private:
	void SetupUI();

	void SetupClient();

	void SubscribeToEvents();

	UIElement* ui_root;

	SharedPtr<MenuBuilder> menu;

	SharedPtr<Client> client;
	SharedPtr<Server> server;
};

//DEFINEs
#define SERVER_PORT 4826