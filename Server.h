#pragma once

#include <Urho3D/Core/Object.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Network/Network.h>

using namespace Urho3D;

class Server : public Object {
	URHO3D_OBJECT(Server, Object);

public:
	Server(Context* context);
	~Server();

	static void RegisterObject(Context* context);

	void Start();
	void Stop();

private:
	void SetupScene();
	void SetupNetwork();

	Node* AddShip(Connection* owner, Vector3 position);

	void SubscribeToEvents();

	void onClientConnected(StringHash eventType, VariantMap& eventData);
	void onClientSceneLoaded(StringHash eventType, VariantMap& eventData);
	void onClientDisconnected(StringHash eventType, VariantMap& eventData);

	void onBeginGame(StringHash eventType, VariantMap& eventData);
	void onShipTurn(StringHash eventType, VariantMap& eventData);
	void onShipThrottle(StringHash eventType, VariantMap& eventData);
	void onShipFireCannon(StringHash eventType, VariantMap& eventData);

	Network* network;

	HashMap<Connection*, Node*> ships;

	SharedPtr<Scene> scene_;

	bool playing = false;
};