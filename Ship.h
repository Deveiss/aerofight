#pragma once

#include <Urho3D/Scene/LogicComponent.h>

using namespace Urho3D;

class Ship : public LogicComponent {
	URHO3D_OBJECT(Ship, LogicComponent);

public:
	Ship(Context* context);

	static void RegisterObject(Context* context);

	void SubscribeToEvents();

	virtual void Start();
	virtual void Update(float timestep);

	void FireCannon();
	void SetSpeed(float newspeed) { speed = MIN_SPEED + ((MAX_SPEED - MIN_SPEED) * newspeed); }

private:
	float speed = 1.f;
	bool left = true;

	float reloadTime = 0.f;

	const float MAX_RELOAD_TIME = 0.1;
	const float MIN_SPEED = 0.1;
	const float MAX_SPEED = 1.5;
};