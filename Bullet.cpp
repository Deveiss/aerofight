#include "Bullet.h"

#include "Aerofight.h"
#include "AerofightEvents.h"

using namespace Urho3D;

Bullet::Bullet(Context* context) : LogicComponent(context) {
}

void Bullet::RegisterObject(Context* context) {
	context->RegisterFactory<Bullet>();
}

void Bullet::Start() {
	SubscribeToEvents();
}

void Bullet::Update(float timestep) {
	GetNode()->Translate(Vector3::FORWARD * timestep * BULLET_SPEED);

	lifetime += timestep;
	if (lifetime >= maxLifetime)
		GetNode()->Remove();
}

void Bullet::SubscribeToEvents() {

}