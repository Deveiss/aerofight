#include "Server.h"

#include <Urho3D/IO/Log.h>
#include <Urho3D/Network/Network.h>
#include <Urho3D/Network/NetworkEvents.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Skybox.h>
#include <Urho3D/Resource/ResourceCache.h>

#include "Aerofight.h"
#include "AerofightEvents.h"
#include "Ship.h"

using namespace Urho3D;

Server::Server(Context* context) : Object(context) {
}

Server::~Server() {
}

void Server::RegisterObject(Context* context) {
	context->RegisterFactory<Server>();
}

void Server::Start() {
	SetupScene();
	SetupNetwork();

	SubscribeToEvents();
}

void Server::Stop() {
	GetSubsystem<Network>()->StopServer();
}

void Server::SetupScene() {
	scene_ = new Scene(context_);

	Node* skyNode = scene_->CreateChild("Sky");
	skyNode->SetScale(500.0f); // The scale actually does not matter
	Skybox* skybox = skyNode->CreateComponent<Skybox>();
	skybox->SetModel(GetSubsystem<ResourceCache>()->GetResource<Model>("Models/Box.mdl"));
	skybox->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Skybox.xml"));

	Node* box_node = scene_->CreateChild("Box");
	box_node->SetPosition(Vector3(0, 0 ,0));
	box_node->SetRotation(Quaternion(0.0f, 0.0f, 0.0f));
	box_node->SetScale(0.5f + Random(2.0f));
	StaticModel* box_model = box_node->CreateComponent<StaticModel>();
	box_model->SetModel(GetSubsystem<ResourceCache>()->GetResource<Model>("Models/Box.mdl"));
	box_model->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/DefaultGrey.xml"));
}

void Server::SetupNetwork() {
	network = GetSubsystem<Network>();

	network->StartServer(SERVER_PORT);
}

Node* Server::AddShip(Connection* owner, Vector3 position) {
	ResourceCache* cache = GetSubsystem<ResourceCache>();

	Node* ship = scene_->CreateChild("ship_node");
	ship->SetPosition(position);
	ship->SetRotation(Quaternion(90, 0, 0));

	Ship* comp = ship->CreateComponent<Ship>();

	StaticModel* model = ship->CreateComponent<StaticModel>();
	model->SetModel(cache->GetResource<Model>("Models/CWU_player-spaceship.mdl"));
	model->ApplyMaterialList();

	ship->SetScale(ship->GetScale() / 6);
	ship->SetOwner(owner);

	ships[owner] = ship;

	VariantMap remoteEventData;
	remoteEventData[afEvents::NewPlayerShip::P_SHIPID] = ship->GetID();
	owner->SendRemoteEvent(afEvents::E_NEW_PLAYER_SHIP, true, remoteEventData);

	return ship;
}

void Server::SubscribeToEvents() {
	SubscribeToEvent(E_CLIENTCONNECTED, URHO3D_HANDLER(Server, onClientConnected));
	SubscribeToEvent(E_CLIENTSCENELOADED, URHO3D_HANDLER(Server, onClientSceneLoaded));
	SubscribeToEvent(E_CLIENTDISCONNECTED, URHO3D_HANDLER(Server, onClientDisconnected));

	SubscribeToEvent(afEvents::E_BEGIN_GAME, URHO3D_HANDLER(Server, onBeginGame));
	SubscribeToEvent(afEvents::E_SHIP_TURN, URHO3D_HANDLER(Server, onShipTurn));
	SubscribeToEvent(afEvents::E_SHIP_THROTTLE, URHO3D_HANDLER(Server, onShipThrottle));
	SubscribeToEvent(afEvents::E_SHIP_FIRE_CANNON, URHO3D_HANDLER(Server, onShipFireCannon));
}

void Server::onClientConnected(StringHash eventType, VariantMap& eventData) {
	Connection* conn = static_cast<Connection*>(eventData[ClientConnected::P_CONNECTION].GetPtr());
	conn->SetScene(scene_);
}

void Server::onClientSceneLoaded(StringHash eventType, VariantMap& eventData) {
	Connection* conn = static_cast<Connection*>(eventData[ClientSceneLoaded::P_CONNECTION].GetPtr());

	if (playing) {
		AddShip(conn, Vector3(0, 10, 0));
		conn->SendRemoteEvent(afEvents::E_GAME_STARTING, true);
	}
}

void Server::onClientDisconnected(StringHash eventType, VariantMap& eventData) {
	Connection* conn = static_cast<Connection*>(eventData[ClientDisconnected::P_CONNECTION].GetPtr());
	if (conn) {
		ships[conn]->Remove();
		ships.Erase(conn);
	}
}

void Server::onBeginGame(StringHash eventType, VariantMap& eventData) {
	network->BroadcastRemoteEvent(afEvents::E_GAME_STARTING, true);
	playing = true;

	for (auto a : network->GetClientConnections()) {
		AddShip(a, Vector3(0, 10, 0));
	}
}

void Server::onShipTurn(StringHash eventType, VariantMap& eventData) {
	if (!playing)
		return;
	Connection* conn = static_cast<Connection*>(eventData[RemoteEventData::P_CONNECTION].GetPtr());

	if (ships[conn]) {
		ships[conn]->Rotate(Quaternion(-(eventData[afEvents::ShipTurn::P_DY].GetFloat()), 0.f, -(eventData[afEvents::ShipTurn::P_DX].GetFloat())), TransformSpace::TS_LOCAL);
	}
	else {
		URHO3D_LOGERROR("ships[conn] FAIL!");

		URHO3D_LOGDEBUG("TESTING: " + String(conn->GetPort()));

		for (auto a : ships) {
			URHO3D_LOGDEBUG(String(a.first_->GetPort()) + " " + String(a.second_->GetID()));
		}
	}
}

void Server::onShipThrottle(StringHash eventType, VariantMap& eventData) {
	if (!playing)
		return;
	Connection* conn = static_cast<Connection*>(eventData[RemoteEventData::P_CONNECTION].GetPtr());

	if (ships[conn]) {
		ships[conn]->GetComponent<Ship>()->SetSpeed(Min(eventData[afEvents::ShipThrottle::P_THROTTLE].GetFloat(), 1));
	}
	else {
		URHO3D_LOGERROR("ships[conn] FAIL onShipTurn!");
	}
}

void Server::onShipFireCannon(StringHash eventType, VariantMap& eventData) {
	Connection* conn = static_cast<Connection*>(eventData[RemoteEventData::P_CONNECTION].GetPtr());
	if (conn) {
		Node* node = ships[conn];
		if (node) {
			Ship* ship = node->GetComponent<Ship>();
			if(ship)
				ship->FireCannon();
		}
	}
}