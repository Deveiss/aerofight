#pragma once

#include <Urho3D/Scene/LogicComponent.h>

using namespace Urho3D;

class Bullet : public LogicComponent {
	URHO3D_OBJECT(Bullet, LogicComponent);

public:
	Bullet(Context* context);

	static void RegisterObject(Context* context);

	void SubscribeToEvents();

	virtual void Start();
	virtual void Update(float timestep);

private:
	float lifetime = 0.f;

	float maxLifetime = 10.f;
	const float BULLET_SPEED = 5.f;
};