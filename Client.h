#pragma once

#include <Urho3D/Core/Object.h>
#include <Urho3D/Scene/Scene.h>

using namespace Urho3D;

class Client : public Object {
	URHO3D_OBJECT(Client, Object);

public:
	Client(Context* context);

	static void RegisterObject(Context* context);

	void Start();
	void Stop();

private:
	void SetupScene();
	void SetupViewport();
	void SetupUI();

	void SubscribeToEvents();

	void onUpdate(StringHash eventType, VariantMap& eventData);
	void onNetworkUpdate(StringHash eventType, VariantMap& eventData);
	void onKeyDown(StringHash eventType, VariantMap& eventData);
	void onServerDisconnected(StringHash eventType, VariantMap& eventData);
	void onTouchMove(StringHash eventType, VariantMap& eventData);
	//void onJoystickAxisMove(StringHash eventType, VariantMap& eventData);

	void onNewPlayerShip(StringHash eventType, VariantMap& eventData);
	void onAttemptConnect(StringHash eventType, VariantMap& eventData);
	void onConnectCancel(StringHash eventType, VariantMap& eventData);
	void onGameStarting(StringHash eventType, VariantMap& eventData);
	void onShipCannonLoaded(StringHash eventType, VariantMap& eventData);

	SharedPtr<Scene> scene_;
	
	Node* cameraNode_ = nullptr;
	Node* ourShip = nullptr;

	bool cannonLoaded = true;

	int connectRetry = 0;

	bool playing = false;

	const int CONNECT_RETRY_MAX = 5;
};