#include "Ship.h"

#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Resource/ResourceCache.h>

#include "Aerofight.h"
#include "AerofightEvents.h"
#include "Bullet.h"

using namespace Urho3D;

Ship::Ship(Context* context) : LogicComponent(context) {
}

void Ship::RegisterObject(Context* context) {
	context->RegisterFactory<Ship>();
}

void Ship::Start() {
	SubscribeToEvents();
}

void Ship::Update(float timestep) {
	GetNode()->Translate(Vector3::FORWARD * timestep * speed, TransformSpace::TS_LOCAL);

	if (reloadTime > 0) {
		reloadTime -= timestep;

		if (reloadTime <= 0)
			GetNode()->GetOwner()->SendRemoteEvent(afEvents::E_SHIP_CANNON_LOADED, true);
	}
}

void Ship::FireCannon() {
	if (reloadTime > 0)
		return;

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	Node* node = GetNode()->GetScene()->CreateChild("bullet_node");

	node->SetRotation(GetNode()->GetRotation());
	node->SetPosition(GetNode()->GetPosition() + (GetNode()->GetDirection() * 0.05));
	if (left)
		node->Translate(Vector3::LEFT * 0.1, TransformSpace::TS_LOCAL);
	else
		node->Translate(Vector3::RIGHT * 0.1, TransformSpace::TS_LOCAL);
	left = !left;

	Bullet* comp = node->CreateComponent<Bullet>();

	StaticModel* model = node->CreateComponent<StaticModel>();
	model->SetModel(cache->GetResource<Model>("Models/Box.mdl"));

	node->SetOwner(GetNode()->GetOwner());

	node->SetScale(node->GetScale() / 25);

	reloadTime = MAX_RELOAD_TIME;
}

void Ship::SubscribeToEvents() {

}