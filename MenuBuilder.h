#pragma once

#include <Urho3D/Core/Object.h>
#include <Urho3D/UI/UIElement.h>

using namespace Urho3D;

class MenuBuilder : public Object {
	URHO3D_OBJECT(MenuBuilder, Object);

public:
	MenuBuilder(Context* context);

	static void RegisterObject(Context* context);

	void Start();

private:
	void DrawBlack();
	void DrawMainMenu();
	void DrawConnect();
	void DrawLobbyClient();
	void DrawLobbyServer();

	void onMainMenuJoinGamePressed(StringHash eventType, VariantMap& eventData);
	void onMainMenuHostGamePressed(StringHash eventType, VariantMap& eventData);

	void onConnectButtonPressed(StringHash eventType, VariantMap& eventData);
	void onConnectBackButtonPressed(StringHash eventType, VariantMap& eventData);

	void onLobbyClientReadyPressed(StringHash eventType, VariantMap& eventData);
	void onLobbyClientBackPressed(StringHash eventType, VariantMap& eventData);

	void onLobbyServerStartPressed(StringHash eventType, VariantMap& eventData);
	void onLobbyServerBackPressed(StringHash eventType, VariantMap& eventData);

	void SubscribeToEvents();

	void onConnectFailed(StringHash eventType, VariantMap& eventData);
	void onServerConnected(StringHash eventType, VariantMap& eventData);
	void onClientConnected(StringHash eventType, VariantMap& eventData);
	void onClientDisconnected(StringHash eventType, VariantMap& eventData);

	void onGameStarting(StringHash eventType, VariantMap& eventData);

private:
	UIElement* ui_root;
};