#include "Aerofight.h"

#include <Urho3D/Urho3D.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Network/Network.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/LineEdit.h>

#include "AerofightEvents.h"

#include "MenuBuilder.h"

#include "Ship.h"
#include "Bullet.h"

#include "Client.h"
#include "Server.h"

using namespace Urho3D;

void Aerofight::Setup() {
	engineParameters_["FullScreen"] = false;
	engineParameters_["WindowResizable"] = true;
	engineParameters_["WindowWidth"] = 800;
	engineParameters_["WindowHeight"] = 600;
	engineParameters_["WindowTitle"] = "Aerofight - early dev";
	engineParameters_["LogLevel"] = LOG_DEBUG;
//	engineParameters_["FrameLimiter"] = true;
	engineParameters_["TouchEmulation"] = true;

//	engine_->SetMaxFps(10);

	MenuBuilder::RegisterObject(context_);

	Ship::RegisterObject(context_);
	Bullet::RegisterObject(context_);

	Server::RegisterObject(context_);
	Client::RegisterObject(context_);

	GetSubsystem<Network>()->RegisterRemoteEvent(afEvents::E_PLAYER_READY);
	GetSubsystem<Network>()->RegisterRemoteEvent(afEvents::E_SHIP_FIRE_CANNON);
	GetSubsystem<Network>()->RegisterRemoteEvent(afEvents::E_SHIP_TURN);
	GetSubsystem<Network>()->RegisterRemoteEvent(afEvents::E_SHIP_THROTTLE);
	GetSubsystem<Network>()->RegisterRemoteEvent(afEvents::E_SHIP_CANNON_LOADED);

	GetSubsystem<Network>()->RegisterRemoteEvent(afEvents::E_NEW_PLAYER_SHIP);
	GetSubsystem<Network>()->RegisterRemoteEvent(afEvents::E_GAME_STARTING);
}

void Aerofight::Start() {
	context_->RegisterSubsystem(this);

	OpenConsoleWindow();
	
	SubscribeToEvents();

	SetupUI();
	SetupClient();

	menu->Start();
}

void Aerofight::Stop() {
	engine_->DumpResources(true);
}


void Aerofight::SetupUI () {
	menu = context_->CreateObject<MenuBuilder>();
	ui_root = GetSubsystem<UI>()->GetRoot();
}


void Aerofight::SetupClient() {
	client = context_->CreateObject<Client>();
	client->Start();
}


void Aerofight::SetupServer() {
	server = context_->CreateObject<Server>();
	server->Start();
}

void Aerofight::DestroyServer() {
	server->Stop();
	server = nullptr;
}


void Aerofight::SubscribeToEvents() {
	
}

URHO3D_DEFINE_APPLICATION_MAIN(Aerofight);