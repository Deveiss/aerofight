#include "MenuBuilder.h"

#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/UI/UIElement.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/BorderImage.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Network/NetworkEvents.h>
#include <Urho3D/IO/Log.h>

#include "Aerofight.h"
#include "AerofightEvents.h"

MenuBuilder::MenuBuilder(Context* context) : Object(context) {}

void MenuBuilder::Start() {
	ui_root = GetSubsystem<UI>()->GetRoot();
	ui_root->SetDefaultStyle(GetSubsystem<ResourceCache>()->GetResource<XMLFile>("UI/DefaultStyle.xml"));

	SubscribeToEvents();

	DrawMainMenu();
}

void MenuBuilder::RegisterObject(Context* context) {
	context->RegisterFactory<MenuBuilder>();

	//URHO3D_ATTRIBUTE("name", type, variable, defaultValue, AM_DEFAULT);
}

void MenuBuilder::DrawBlack() {
	BorderImage* background = new BorderImage(context_);
	background->SetName("background");
	background->SetColor(Urho3D::Color(0.f, 0.f, 0.f, 1.f));
	background->SetHorizontalAlignment(HA_CENTER);
	background->SetVerticalAlignment(VA_CENTER);
	background->SetSize(ui_root->GetSize());
	background->SetPosition(0, 0);

	ui_root->AddChild(background);
}

void MenuBuilder::DrawMainMenu() {
	UIElement* mainMenu = new UIElement(context_);
	mainMenu->SetName("mainMenu");
	mainMenu->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	mainMenu->SetSize(ui_root->GetSize());

	Text* titleText = new Text(context_);
	titleText->SetName("titleText");
	titleText->SetStyleAuto(ui_root->GetDefaultStyle());
	titleText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 50);
	titleText->SetText("Aerofight");
	titleText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	titleText->SetColor(Color::WHITE);
	titleText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	titleText->SetPosition(0, -(mainMenu->GetHeight() / 6));

	Button* hostGameButton = new Button(context_);
	hostGameButton->SetName("hostGameButton");
	hostGameButton->SetStyleAuto(ui_root->GetDefaultStyle());
	hostGameButton->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	hostGameButton->SetPosition(-(mainMenu->GetWidth() / 4), mainMenu->GetHeight() / 6);
	hostGameButton->SetSize(mainMenu->GetWidth() / 3, mainMenu->GetHeight() / 6);

	Text* hostGameText = new Text(context_);
	hostGameText->SetName("hostGameText");
	hostGameText->SetStyleAuto(ui_root->GetDefaultStyle());
	hostGameText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	hostGameText->SetText("Host a Game");
	hostGameText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	hostGameText->SetColor(Color::WHITE);
	hostGameText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);

	hostGameButton->AddChild(hostGameText);

	Button* joinGameButton = new Button(context_);
	joinGameButton->SetName("joinGameButton");
	joinGameButton->SetStyleAuto(ui_root->GetDefaultStyle());
	joinGameButton->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	joinGameButton->SetPosition(mainMenu->GetWidth() / 4, mainMenu->GetHeight() / 6);
	joinGameButton->SetSize(mainMenu->GetWidth() / 3, mainMenu->GetHeight() / 6);

	Text* joinGameText = new Text(context_);
	joinGameText->SetName("joinGameText");
	joinGameText->SetStyleAuto(ui_root->GetDefaultStyle());
	joinGameText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	joinGameText->SetText("Join a Game");
	joinGameText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	joinGameText->SetColor(Color::WHITE);
	joinGameText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);

	joinGameButton->AddChild(joinGameText);

	mainMenu->AddChild(titleText);
	mainMenu->AddChild(hostGameButton);
	mainMenu->AddChild(joinGameButton);

	ui_root->AddChild(mainMenu);

	SubscribeToEvent(hostGameButton, E_RELEASED, URHO3D_HANDLER(MenuBuilder, onMainMenuHostGamePressed));
	SubscribeToEvent(joinGameButton, E_RELEASED, URHO3D_HANDLER(MenuBuilder, onMainMenuJoinGamePressed));
}

void MenuBuilder::DrawConnect() {
	UIElement* connectMenu = new UIElement(context_);
	connectMenu->SetName("connectMenu");
	connectMenu->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	connectMenu->SetSize(ui_root->GetSize());

	Text* instructionsText = new Text(context_);
	instructionsText->SetName("instructionsText");
	instructionsText->SetStyleAuto(ui_root->GetDefaultStyle());
	instructionsText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	instructionsText->SetText("Enter the IP address of the host:");
	instructionsText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	instructionsText->SetColor(Color::WHITE);
	instructionsText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	instructionsText->SetPosition(0, -(connectMenu->GetHeight() / 12));

	LineEdit* ipLineEdit = new LineEdit(context_);
	ipLineEdit->SetName("ipLineEdit");
	ipLineEdit->SetStyleAuto(ui_root->GetDefaultStyle());
	ipLineEdit->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	ipLineEdit->SetPosition(0, 0);
	ipLineEdit->SetSize(connectMenu->GetWidth() / 1.5, connectMenu->GetHeight() / 12);
	ipLineEdit->SetTextSelectable(true);
	ipLineEdit->SetTextCopyable(true);
	ipLineEdit->GetTextElement()->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	ipLineEdit->GetTextElement()->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	ipLineEdit->GetTextElement()->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);

	Button* connectButton = new Button(context_);
	connectButton->SetName("connectButton");
	connectButton->SetStyleAuto(ui_root->GetDefaultStyle());
	connectButton->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	connectButton->SetPosition(0, connectMenu->GetHeight() / 6);
	connectButton->SetSize(connectMenu->GetWidth() / 3, connectMenu->GetHeight() / 6);

	Text* connectText = new Text(context_);
	connectText->SetName("connectText");
	connectText->SetStyleAuto(ui_root->GetDefaultStyle());
	connectText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	connectText->SetText("Connect!");
	connectText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	connectText->SetColor(Color::WHITE);
	connectText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);

	//connectButton->SetSize(connectText->GetSize() * 2);
	connectButton->AddChild(connectText);

	Button* backButton = new Button(context_);
	backButton->SetName("backButton");
	backButton->SetStyleAuto(ui_root->GetDefaultStyle());
	backButton->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	backButton->SetSize(connectMenu->GetWidth() / 4, connectMenu->GetHeight() / 8);
	//backButton->SetPosition(-((connectMenu->GetWidth() / 2) - backButton->GetWidth()), (connectMenu->GetHeight() / 2) - backButton->GetHeight());
	backButton->SetPosition(-(connectMenu->GetWidth() / 3), connectMenu->GetHeight() / 2.5);

	Text* backText = new Text(context_);
	backText->SetName("backText");
	backText->SetStyleAuto(ui_root->GetDefaultStyle());
	backText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	backText->SetText("Back");
	backText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	backText->SetColor(Color::WHITE);
	backText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);

	backButton->AddChild(backText);

	connectMenu->AddChild(instructionsText);
	connectMenu->AddChild(ipLineEdit);
	connectMenu->AddChild(connectButton);
	connectMenu->AddChild(backButton);

	ui_root->AddChild(connectMenu);

	SubscribeToEvent(connectButton, E_RELEASED, URHO3D_HANDLER(MenuBuilder, onConnectButtonPressed));
	SubscribeToEvent(backButton, E_RELEASED, URHO3D_HANDLER(MenuBuilder, onConnectBackButtonPressed));
}

void MenuBuilder::DrawLobbyClient() {
	UIElement* lobbyClientMenu = new UIElement(context_);
	lobbyClientMenu->SetName("lobbyClientMenu");
	lobbyClientMenu->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	lobbyClientMenu->SetSize(ui_root->GetSize());

	Button* backButton = new Button(context_);
	backButton->SetName("backButton");
	backButton->SetStyleAuto(ui_root->GetDefaultStyle());
	backButton->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	backButton->SetSize(lobbyClientMenu->GetWidth() / 4, lobbyClientMenu->GetHeight() / 8);
	//backButton->SetPosition(-((connectMenu->GetWidth() / 2) - backButton->GetWidth()), (connectMenu->GetHeight() / 2) - backButton->GetHeight());
	backButton->SetPosition(-(lobbyClientMenu->GetWidth() / 3), lobbyClientMenu->GetHeight() / 2.5);

	Text* backText = new Text(context_);
	backText->SetName("backText");
	backText->SetStyleAuto(ui_root->GetDefaultStyle());
	backText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	backText->SetText("Back");
	backText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	backText->SetColor(Color::WHITE);
	backText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);

	backButton->AddChild(backText);

	lobbyClientMenu->AddChild(backButton);

	ui_root->AddChild(lobbyClientMenu);

	SubscribeToEvent(backButton, E_RELEASED, URHO3D_HANDLER(MenuBuilder, onLobbyClientBackPressed));
}

void MenuBuilder::DrawLobbyServer() {
	UIElement* lobbyServerMenu = new UIElement(context_);
	lobbyServerMenu->SetName("lobbyServerMenu");
	lobbyServerMenu->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	lobbyServerMenu->SetSize(ui_root->GetSize());

	Text* playersList = new Text(context_);
	playersList->SetName("playersList");
	playersList->SetStyleAuto(ui_root->GetDefaultStyle());
	playersList->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	playersList->SetText("No other players.");
	playersList->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	playersList->SetColor(Color::WHITE);
	playersList->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	playersList->SetPosition(0, -(lobbyServerMenu->GetHeight() / 4));

	Button* startButton = new Button(context_);
	startButton->SetName("startButton");
	startButton->SetStyleAuto(ui_root->GetDefaultStyle());
	startButton->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	startButton->SetSize(lobbyServerMenu->GetWidth() / 3, lobbyServerMenu->GetHeight() / 6);
	//backButton->SetPosition(-((connectMenu->GetWidth() / 2) - backButton->GetWidth()), (connectMenu->GetHeight() / 2) - backButton->GetHeight());
	startButton->SetPosition(0, lobbyServerMenu->GetHeight() / 4);

	Text* startText = new Text(context_);
	startText->SetName("backText");
	startText->SetStyleAuto(ui_root->GetDefaultStyle());
	startText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	startText->SetText("Start Game!");
	startText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	startText->SetColor(Color::WHITE);
	startText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);

	startButton->AddChild(startText);

	Button* backButton = new Button(context_);
	backButton->SetName("backButton");
	backButton->SetStyleAuto(ui_root->GetDefaultStyle());
	backButton->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);
	backButton->SetSize(lobbyServerMenu->GetWidth() / 4, lobbyServerMenu->GetHeight() / 8);
	//backButton->SetPosition(-((connectMenu->GetWidth() / 2) - backButton->GetWidth()), (connectMenu->GetHeight() / 2) - backButton->GetHeight());
	backButton->SetPosition(-(lobbyServerMenu->GetWidth() / 3), lobbyServerMenu->GetHeight() / 2.5);

	Text* backText = new Text(context_);
	backText->SetName("backText");
	backText->SetStyleAuto(ui_root->GetDefaultStyle());
	backText->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 20);
	backText->SetText("Back");
	backText->SetTextAlignment(HorizontalAlignment::HA_CENTER);
	backText->SetColor(Color::WHITE);
	backText->SetAlignment(HorizontalAlignment::HA_CENTER, VerticalAlignment::VA_CENTER);

	backButton->AddChild(backText);


	lobbyServerMenu->AddChild(playersList);
	lobbyServerMenu->AddChild(startButton);
	lobbyServerMenu->AddChild(backButton);

	ui_root->AddChild(lobbyServerMenu);

	SubscribeToEvent(startButton, E_RELEASED, URHO3D_HANDLER(MenuBuilder, onLobbyServerStartPressed));
	SubscribeToEvent(backButton, E_RELEASED, URHO3D_HANDLER(MenuBuilder, onLobbyServerBackPressed));
}





void MenuBuilder::onMainMenuJoinGamePressed(StringHash eventType, VariantMap& eventData) {
	ui_root->RemoveAllChildren();
	DrawBlack();
	DrawConnect();
}

void MenuBuilder::onMainMenuHostGamePressed(StringHash eventType, VariantMap& eventData) {
	ui_root->RemoveAllChildren();
	DrawBlack();
	DrawLobbyServer();

	context_->GetSubsystem<Aerofight>()->SetupServer();

	VariantMap remoteData;
	remoteData[afEvents::AttemptConnect::P_ADDRESS] = "localhost";
	SendEvent(afEvents::E_ATTEMPT_CONNECT, remoteData);
}


void MenuBuilder::onConnectButtonPressed(StringHash eventType, VariantMap& eventData) {
	LineEdit* ipLineEdit = static_cast<LineEdit*>(ui_root->GetChild(String("connectMenu"))->GetChild(String("ipLineEdit")));
	
	if (ipLineEdit) {
		VariantMap data;
		data[afEvents::AttemptConnect::P_ADDRESS] = ipLineEdit->GetText().Trimmed();
		SendEvent(afEvents::E_ATTEMPT_CONNECT, data);
	}

	Text* instructionsText = static_cast<Text*>(ui_root->GetChild(String("connectMenu"))->GetChild(String("instructionsText")));

	if (instructionsText) {
		instructionsText->SetColor(Color::WHITE);
		instructionsText->SetText("Connecting...");
	}

	Button* connectButton = static_cast<Button*>(ui_root->GetChild(String("connectMenu"))->GetChild(String("connectButton")));

	if (connectButton) {
		connectButton->SetEditable(false);
	}
}

void MenuBuilder::onConnectBackButtonPressed(StringHash eventType, VariantMap& eventData) {
	ui_root->RemoveAllChildren();
	DrawBlack();
	DrawMainMenu();

	SendEvent(afEvents::E_CONNECT_CANCEL);
}


void MenuBuilder::onLobbyClientReadyPressed(StringHash eventType, VariantMap& eventData) {

}

void MenuBuilder::onLobbyClientBackPressed(StringHash eventType, VariantMap& eventData) {
	ui_root->RemoveAllChildren();
	DrawBlack();
	DrawMainMenu();

	SendEvent(afEvents::E_CONNECT_CANCEL);
}


void MenuBuilder::onLobbyServerStartPressed(StringHash eventType, VariantMap& eventData) {
	ui_root->RemoveAllChildren();
	
	SendEvent(afEvents::E_BEGIN_GAME);
}

void MenuBuilder::onLobbyServerBackPressed(StringHash eventType, VariantMap& eventData) {
	ui_root->RemoveAllChildren();
	DrawBlack();
	DrawMainMenu();

	context_->GetSubsystem<Aerofight>()->DestroyServer();
}

void MenuBuilder::SubscribeToEvents() {
	SubscribeToEvent(E_CONNECTFAILED, URHO3D_HANDLER(MenuBuilder, onConnectFailed));
	SubscribeToEvent(E_SERVERCONNECTED, URHO3D_HANDLER(MenuBuilder, onServerConnected));
	SubscribeToEvent(E_CLIENTCONNECTED, URHO3D_HANDLER(MenuBuilder, onClientConnected));
	SubscribeToEvent(E_CLIENTDISCONNECTED, URHO3D_HANDLER(MenuBuilder, onClientDisconnected));

	SubscribeToEvent(afEvents::E_GAME_STARTING, URHO3D_HANDLER(MenuBuilder, onGameStarting));
}

void MenuBuilder::onConnectFailed(StringHash eventType, VariantMap& eventData) {
	if (ui_root->GetChild(String("connectMenu"))) {
		Text* instructionsText = static_cast<Text*>(ui_root->GetChild(String("connectMenu"))->GetChild(String("instructionsText")));
		
		if (instructionsText) {
			instructionsText->SetColor(Color::RED);
			instructionsText->SetText("Connect failed!");
		}

		Button* connectButton = static_cast<Button*>(ui_root->GetChild(String("connectMenu"))->GetChild(String("connectButton")));

		if (connectButton) {
			connectButton->SetEditable(true);
		}
	}
}

void MenuBuilder::onServerConnected(StringHash eventType, VariantMap& eventData) {
	if (!GetSubsystem<Network>()->IsServerRunning()) {
		ui_root->RemoveAllChildren();
		DrawBlack();
		DrawLobbyClient();
	}
}

void MenuBuilder::onClientConnected(StringHash eventType, VariantMap& eventData) {
	if (ui_root->GetChild(String("lobbyServerMenu"))) {
		Text* playersList = static_cast<Text*>(ui_root->GetChild(String("lobbyServerMenu"))->GetChild(String("playersList")));
		if (playersList) {
			String playersString = "";

			for (auto c : GetSubsystem<Network>()->GetClientConnections()) {
				playersString.Append(String(c->GetPort()) + "\n");
			}

			playersList->SetText(playersString);
		}
	}
}

void MenuBuilder::onClientDisconnected(StringHash eventType, VariantMap& eventData) {
	if (ui_root->GetChild(String("lobbyServerMenu"))) {
		Text* playersList = static_cast<Text*>(ui_root->GetChild(String("lobbyServerMenu"))->GetChild(String("playersList")));
		if (playersList) {
			String playersString = "";

			for (auto c : GetSubsystem<Network>()->GetClientConnections()) {
				if(c != eventData[ClientDisconnected::P_CONNECTION].GetPtr())
					playersString.Append(String(c->GetPort()) + "\n");
			}

			if(!playersString.Empty())
				playersList->SetText(playersString);
			else
				playersList->SetText("No other players.");
		}
	}
}

void MenuBuilder::onGameStarting(StringHash eventType, VariantMap& eventData) {
	ui_root->RemoveAllChildren();
}