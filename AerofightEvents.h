#include <Urho3D\Core\Object.h>

namespace afEvents {
	// Local Events
	//

	// UI -> Client : Client would like to attempt to connect to the specified server.
	URHO3D_EVENT(E_ATTEMPT_CONNECT, AttemptConnect) {
		URHO3D_PARAM(P_ADDRESS, Addresss);			// string
	}

	// UI -> Client : Client would like to cancel his connection to the server.
	URHO3D_EVENT(E_CONNECT_CANCEL, ConnectCancel) {}

	// UI -> Server : The player has indicated that it is time to start the game.
	URHO3D_EVENT(E_BEGIN_GAME, BeginGame) {}

	// Ship -> Server : Used to indicate the SENDER has died, and it's references should be removed.
	URHO3D_EVENT(E_SHIP_DEATH, ShipDeath) {}


	// Networked Events
	//

	// Client -> Server : Signal that a player is ready for the game to begin.
	URHO3D_EVENT(E_PLAYER_READY, PlayerReady) {}

	// Client -> Server : Fire bullets out of the ships cannons.
	URHO3D_EVENT(E_SHIP_FIRE_CANNON, ShipFire) {}

	// Client -> Server : Set the desired throttle of the ship.
	URHO3D_EVENT(E_SHIP_THROTTLE, ShipThrottle) {
		URHO3D_PARAM(P_THROTTLE, Throttle);			// float
	}

	// Client -> Server : Sets the delta rotation of the ship.
	URHO3D_EVENT(E_SHIP_TURN, ShipTurn) {
		URHO3D_PARAM(P_DX, dX);						// float
		URHO3D_PARAM(P_DY, dY);						// float
		URHO3D_PARAM(P_DZ, dZ);						// float
	}


	// Server -> Client : Informs the client that the ships cannons are ready to fire.
	URHO3D_EVENT(E_SHIP_CANNON_LOADED, ShipFire) {}

	// Server -> Client : Register a new player-controlled ship with the client.
	URHO3D_EVENT(E_NEW_PLAYER_SHIP, NewPlayerShip) {
		URHO3D_PARAM(P_SHIPID, ShipID);				// unsigned int
	}

	// Server -> Client : Informs the client that the game is starting.
	URHO3D_EVENT(E_GAME_STARTING, GameStarting) {}
}