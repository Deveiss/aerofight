#include "Client.h"

#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/Engine/DebugHud.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Network/Network.h>
#include <Urho3D/Network/NetworkEvents.h>
#include <Urho3D/Graphics/Graphics.h>

#include <iostream>
#include <iomanip>

#include "Aerofight.h"
#include "AerofightEvents.h"

using namespace Urho3D;

Client::Client(Context* context) : Object(context) {
}

void Client::RegisterObject(Context* context) {
	context->RegisterFactory<Client>();

	//URHO3D_ATTRIBUTE("name", type, variable, defaultValue, AM_DEFAULT);
}

void Client::Start() {
	SetupScene();
	SetupViewport();
	SetupUI();

	SubscribeToEvents();
}

void Client::Stop() {
}

void Client::SetupScene() {
	scene_ = new Scene(context_);
	
	scene_->CreateComponent<Octree>(LOCAL);

	Node* lightNode = scene_->CreateChild("DirectionalLight", LOCAL);
	lightNode->SetDirection(Vector3(-0.5f, 1.0f, -0.5f));
	Light* light = lightNode->CreateComponent<Light>();
	light->SetLightType(LIGHT_DIRECTIONAL);

	Node* lightNode2 = scene_->CreateChild("DirectionalLight", LOCAL);
	lightNode2->SetDirection(Vector3(0.5f, -1.0f, 0.5f));
	Light* light2 = lightNode2->CreateComponent<Light>();
	light2->SetLightType(LIGHT_DIRECTIONAL);

	cameraNode_ = scene_->CreateChild("Camera", LOCAL);
	Camera* camera = cameraNode_->CreateComponent<Camera>();
	camera->SetFov(90);
	cameraNode_->SetPosition(Vector3(0, 5, 0));
	cameraNode_->SetRotation(Quaternion(90, 0, 0));

	//scene_->SetUpdateEnabled(false);
}

void Client::SetupViewport() {
	Renderer* renderer = GetSubsystem<Renderer>();

	SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
	renderer->SetViewport(0, viewport);
}

void Client::SetupUI() {
	// Get default style
	ResourceCache* cache = GetSubsystem<ResourceCache>();
	XMLFile* xmlFile = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");

	// Create console
	Console* console = context_->GetSubsystem<Engine>()->CreateConsole();
	console->SetDefaultStyle(xmlFile);
	console->GetBackground()->SetOpacity(0.8f);

	// Create debug HUD.
	DebugHud* debugHud = context_->GetSubsystem<Engine>()->CreateDebugHud();
	debugHud->SetDefaultStyle(xmlFile);

	Input* input = GetSubsystem<Input>();
	input->SetMouseGrabbed(true);
	input->SetMouseVisible(true);
	input->SetMouseMode(MouseMode::MM_ABSOLUTE);
}

void Client::SubscribeToEvents() {
	SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(Client, onUpdate));
	SubscribeToEvent(E_NETWORKUPDATE, URHO3D_HANDLER(Client, onNetworkUpdate));
	SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(Client, onKeyDown));
	SubscribeToEvent(E_TOUCHMOVE, URHO3D_HANDLER(Client, onTouchMove));
	//SubscribeToEvent(E_JOYSTICKAXISMOVE, URHO3D_HANDLER(Client, onJoystickAxisMove));
	SubscribeToEvent(E_SERVERDISCONNECTED, URHO3D_HANDLER(Client, onServerDisconnected));

	SubscribeToEvent(afEvents::E_ATTEMPT_CONNECT, URHO3D_HANDLER(Client, onAttemptConnect));
	SubscribeToEvent(afEvents::E_CONNECT_CANCEL, URHO3D_HANDLER(Client, onConnectCancel));

	SubscribeToEvent(afEvents::E_NEW_PLAYER_SHIP, URHO3D_HANDLER(Client, onNewPlayerShip));
	SubscribeToEvent(afEvents::E_GAME_STARTING, URHO3D_HANDLER(Client, onGameStarting));
	SubscribeToEvent(afEvents::E_SHIP_CANNON_LOADED, URHO3D_HANDLER(Client, onShipCannonLoaded));
}

void Client::onUpdate(StringHash eventType, VariantMap& eventData) {
//	Input* input = GetSubsystem<Input>();
//	if (input->GetNumJoysticks() > 0) {
//		JoystickState* joystick = input->GetJoystickByIndex(0);
//		std::cout << std::fixed << std::setprecision(5) << "0 = " << joystick->GetAxisPosition(0) << "\t 1 = " << joystick->GetAxisPosition(1) << "\t 2 = " << joystick->GetAxisPosition(2) << "\t 3 = " << joystick->GetAxisPosition(3) << "\t 4 = " << joystick->GetAxisPosition(4) << std::endl;
//	}
}

void Client::onNetworkUpdate(StringHash eventType, VariantMap& eventData) {
	const float SENSITIVITY = 5.f;
	const float ROLL_SENSITIVITY = 8.f;

	if (playing) {
		Input* input = GetSubsystem<Input>();
		if (input->GetNumJoysticks() <= 0)
			return;

		JoystickState* joystick = input->GetJoystickByIndex(0);
		
		VariantMap remoteData;
		remoteData[afEvents::ShipTurn::P_DX] = joystick->GetAxisPosition(0) * ROLL_SENSITIVITY;
		remoteData[afEvents::ShipTurn::P_DY] = joystick->GetAxisPosition(1) * SENSITIVITY;

		VariantMap remoteData2;
		remoteData2[afEvents::ShipThrottle::P_THROTTLE] = Abs(joystick->GetAxisPosition(2) - 1) / 2;

		Connection* server = GetSubsystem<Network>()->GetServerConnection();

		if (server)
			server->SendRemoteEvent(afEvents::E_SHIP_TURN, false, remoteData);
			server->SendRemoteEvent(afEvents::E_SHIP_THROTTLE, false, remoteData2);
	}
}

void Client::onKeyDown(StringHash eventType, VariantMap& eventData) {
	int key = eventData[KeyDown::P_KEY].GetInt();

	if (key == KEY_F1) {
		GetSubsystem<Console>()->Toggle();
	}
	else if (key == KEY_F2) {
		DebugHud* debugHud = GetSubsystem<DebugHud>();
		if (debugHud->GetMode() == 0)
			debugHud->SetMode(DEBUGHUD_SHOW_ALL);
		else
			debugHud->SetMode(DEBUGHUD_SHOW_NONE);
	}
	else if (key == KEY_Z) {
		if (playing && ourShip && cannonLoaded) {
			Connection* server = GetSubsystem<Network>()->GetServerConnection();
			if (server)
				server->SendRemoteEvent(afEvents::E_SHIP_FIRE_CANNON, false);

			cannonLoaded = false;
		}
	}
	else if (key == KEY_R) {
		Input* input = GetSubsystem<Input>();
		if (input->GetNumJoysticks() > 0) {
			JoystickState* joystick = input->GetJoystickByIndex(0);
			joystick->Reset();
		}
	}
}

void Client::onTouchMove(StringHash eventType, VariantMap& eventData) {
	const float SENSITIVITY = 0.3f;
	
	if (playing) {
		VariantMap remoteData;
		remoteData[afEvents::ShipTurn::P_DX] = eventData[TouchMove::P_DX].GetInt() * SENSITIVITY;
		remoteData[afEvents::ShipTurn::P_DY] = eventData[TouchMove::P_DY].GetInt() * SENSITIVITY;
		Connection* server = GetSubsystem<Network>()->GetServerConnection();
		if (server)
			server->SendRemoteEvent(afEvents::E_SHIP_TURN, false, remoteData);
	}
}

//void Client::onJoystickAxisMove(StringHash eventType, VariantMap& eventData) {
//}

void Client::onServerDisconnected(StringHash eventType, VariantMap& eventData) {
	playing = false;
}

void Client::onNewPlayerShip(StringHash eventType, VariantMap& eventData) {
	Node* newship = scene_->GetNode(eventData[afEvents::NewPlayerShip::P_SHIPID].GetUInt());
	
	if (newship) {
		ourShip = newship;
		cameraNode_->SetParent(newship);
		cameraNode_->SetPosition(Vector3(0, 0.5, -2.5));

		URHO3D_LOGDEBUG("Ship acquired!");
	}
}

void Client::onAttemptConnect(StringHash eventType, VariantMap& eventData) {
	GetSubsystem<Network>()->Connect(eventData[afEvents::AttemptConnect::P_ADDRESS].GetString(), SERVER_PORT, scene_);
}

void Client::onConnectCancel(StringHash eventType, VariantMap& eventData) {
	if(GetSubsystem<Network>()->GetServerConnection())
		GetSubsystem<Network>()->Disconnect();
}

void Client::onGameStarting(StringHash eventType, VariantMap& eventData) {
	//scene_->SetUpdateEnabled(true);
	playing = true;
}

void Client::onShipCannonLoaded(StringHash eventType, VariantMap& eventData) {
	cannonLoaded = true;

	if (playing && ourShip && GetSubsystem<Input>()->GetKeyDown(KEY_Z)) {
		Connection* server = GetSubsystem<Network>()->GetServerConnection();
		if (server)
			server->SendRemoteEvent(afEvents::E_SHIP_FIRE_CANNON, false);
	}
}